#===============================================================================
# FHI-aims file: control.in
# Created using the Atomic Simulation Environment (ASE)
# Thu Nov 23 15:54:37 2023
#===============================================================================
xc                                 hse06 0.11
hse_unit                           bohr
relativistic                       atomic_zora scalar
k_grid                             6 6 6
output                             band   0.00000  0.00000  0.00000   0.50000  0.00000  0.50000   46 G  X  
output                             band   0.50000  0.00000  0.50000   0.50000  0.25000  0.75000   23 X  W  
output                             band   0.50000  0.25000  0.75000   0.37500  0.37500  0.75000   16 W  K  
output                             band   0.37500  0.37500  0.75000   0.00000  0.00000  0.00000   49 K  G  
output                             band   0.00000  0.00000  0.00000   0.50000  0.50000  0.50000   40 G  L  
output                             band   0.50000  0.50000  0.50000   0.62500  0.25000  0.62500   28 L  U  
output                             band   0.62500  0.25000  0.62500   0.50000  0.25000  0.75000   16 U  W  
output                             band   0.50000  0.25000  0.75000   0.50000  0.50000  0.50000   33 W  L  
output                             band   0.50000  0.50000  0.50000   0.37500  0.37500  0.75000   28 L  K  
output                             band   0.62500  0.25000  0.62500   0.50000  0.00000  0.50000   16 U  X  
exx_band_structure_version         1
charge_mix_param                   0.3
#===============================================================================

################################################################################
#
#  FHI-aims code project
#  VB, Fritz-Haber Institut, 2009
#
#  Suggested "light" defaults for Li atom (to be pasted into control.in file)
#  Be sure to double-check any results obtained with these settings for post-processing,
#  e.g., with the "tight" defaults and larger basis sets.
#
################################################################################
  species        Li
#     global species definitions
    nucleus             3
    mass                6.941
#
    l_hartree           4
#
    cut_pot             3.5  1.5  1.0
    basis_dep_cutoff    1e-4
#     
    radial_base         29 5.0
    radial_multiplier   1
    angular_grids       specified
      division   0.4484  110
      division   0.5659  194
      division   0.6315  302
#      division   0.6662  434
#      division   0.8186  590
#      division   0.9037  770
#      division   6.2760  974
#      outer_grid   974
      outer_grid   302
################################################################################
#
#  Definition of "minimal" basis
#
################################################################################
#     valence basis states
    valence      2  s   1.
#     ion occupancy
    ion_occ      1  s   2.
################################################################################
#
#  Suggested additional basis functions. For production calculations, 
#  uncomment them one after another (the most important basis functions are
#  listed first).
#
#  Constructed for dimers: 1.80 A, 2.25 A, 2.75 A, 3.50 A, 4.50 A
#
################################################################################
#  "First tier" - improvements: -189.23 meV to -6.35 meV
     hydro 2 p 1.6
     hydro 2 s 2
     hydro 3 d 2.6
#  "Second tier" - improvements: -4.69 meV to -0.41 meV
#     hydro 3 p 4.6
#     hydro 2 p 1.8
#     hydro 3 s 6.2
#     hydro 4 d 4.7
#     hydro 4 f 4.1
#  "Third tier" - improvements: -0.20 meV to -0.15 meV
#     hydro 4 d 0.95
#     hydro 3 p 6.2
#     hydro 3 s 1.7
#  Further functions, listed for completeness
#     VB: The following functions are only listed for completeness; test very
#         carefully before any kind of production use. From the point of view 
#         of the basis construction, their main role is to fill up space, 
#         and they are solely determined by the location of the cutoff potential.
#     hydro 3 p 0.1  # -0.15 meV
#     hydro 4 d 5    # -0.12 meV
#     hydro 4 f 0.1  # -0.14 meV
#     hydro 5 g 0.1  # -0.06 meV


################################################################################
#
#  FHI-aims code project
#  VB, Fritz-Haber Institut, 2009
#
#  Suggested "light" defaults for F atom (to be pasted into control.in file)
#  Be sure to double-check any results obtained with these settings for post-processing,
#  e.g., with the "tight" defaults and larger basis sets.
#
################################################################################
  species        F
#     global species definitions
    nucleus             9
    mass                18.9984032
#
    l_hartree           4
#
    cut_pot             3.5  1.5  1.0
    basis_dep_cutoff    1e-4
#
    radial_base         37 5.0
    radial_multiplier   1
    angular_grids specified 
      division   0.4014  110
      division   0.5291  194
      division   0.6019  302
#      division   0.6814  434
#      division   0.7989  590
#      division   0.8965  770
#      division   1.3427  974
#      outer_grid   974
      outer_grid   302
################################################################################
#
#  Definition of "minimal" basis
#
################################################################################
#     valence basis states
    valence      2  s   2.
    valence      2  p   5.
#     ion occupancy
    ion_occ      2  s   1.
    ion_occ      2  p   4.
################################################################################
#
#  Suggested additional basis functions. For production calculations, 
#  uncomment them one after another (the most important basis functions are
#  listed first).
#
#  Constructed for dimers: 1.2 A, 1.418 A, 1.75 A, 2.25 A, 3.25 A
#
################################################################################
#  "First tier" - improvements: -149.44 meV to -45.88 meV
     hydro 2 p 1.7
     hydro 3 d 7.4
     hydro 3 s 6.8
#  "Second tier" - improvements: -12.96 meV to -1.56 meV
#     hydro 4 f 11.2
#     ionic 2 p auto
#     hydro 1 s 0.75
#     hydro 4 d 8.8
#     hydro 5 g 16.8
#  "Third tier" - improvements: -0.58 meV to -0.05 meV
#     hydro 3 p 6.2
#     hydro 3 s 3.2
#     hydro 4 f 9.6
#     hydro 3 s 19.6
#     hydro 4 d 8.6
#     hydro 5 g 14.4
# Further basis functions: -0.05 meV and below
#     hydro 3 p 4.2
